

export function fight(firstFighter, secondFighter) {
  // return winner
  
  let playerOneHealth = firstFighter.health;
  let playerTwoHealth = secondFighter.health;
  let winner = false;
  let fight = true;

  while (fight) {
    playerOneHealth -= getDamage(secondFighter, firstFighter);
    playerTwoHealth -= getDamage(firstFighter, secondFighter);
    
    if (playerOneHealth <= 0) {
      winner = secondFighter;
      break;
    }
    
    if (playerTwoHealth <= 0) {
      winner = firstFighter;
      break;
    }
  }
  
  return winner;
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage 
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() * (2 - 1) + 1;
  const power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() * (2 - 1) + 1;
  const power = fighter.defense * dodgeChance;
  return power;
}
