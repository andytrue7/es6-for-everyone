import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';

export  function showWinnerModal(fighter) {
  // show winner name and image
  const { name } = fighter;
  const { source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const sourceElement = createElement({ tagName: 'img', className: 'fighter-source' });

  nameElement.innerText = name;
  sourceElement.src =source;

  fighterDetails.append(nameElement);
  fighterDetails.append(sourceElement);

  const title = 'Winner';
  const bodyElement = fighterDetails;
  showModal({ title, bodyElement });
}