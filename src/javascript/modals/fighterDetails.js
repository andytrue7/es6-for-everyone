import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { fight } from '../fight';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name } = fighter;
  const { health } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;
  const { source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });
  const sourceElement = createElement({ tagName: 'img', className: 'fighter-source' });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  healthElement.innerText = health;
  attackElement.innerText = attack;
  defenseElement.innerText = defense;
  sourceElement.src =source;

  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(sourceElement);

  return fighterDetails;
}
